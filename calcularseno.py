# Calcular seno
# Por Rafael Aquino

import math 

def seno(x):
  """ Funcion que calcula el seno de un entero positivo"""
  return math.sin(x)
  
sw = True
while sw:
  numero = int(input("Introduzca un número entero positivo: "))
  if numero < 0:
    print("El numero debe ser mayor a 0 ")
  else:
    sw = False

if not(sw):
  respuesta = seno(numero)
  print(round(respuesta,2))
